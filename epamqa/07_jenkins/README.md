# Jenkins vs Docker

![Self Isolat](https://i.redd.it/bgv83pbo5no41.jpg)

## [01] Setup Jenkins in docker

Simple usage:
```
docker run --name myjenkins -p 8000:8080 jenkins/jenkins:lts

docker exec myjenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

Make it reproducible!
- With some help: https://github.com/jenkinsci/docker/blob/master/README.md

- Base image: jenkins/jenkins:lts
- Customize it:
  - **Preinstall plugins**
    
    plugins.txt
    ```
    cucumber-testresult-plugin:0.8.2
    htmlpublisher:1.22
    docker-commons:1.16
    docker-java-api:3.0.14
    docker-plugin:1.1.9
    job-dsl:1.77
    ...
    ```
    Collect it from an existing Jenkins
    ```
    JENKINS_HOST=username:password@myhost.com:port
    
    curl -sSL "http://$JENKINS_HOST/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/'
    ```

    Dockerfile
    ```
    FROM jenkins/jenkins:lts

    COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
    RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
    ```
  - Disable startup wizard (WARNING: unsecured setup!)
    ```
    ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
    ```
  - Add initializer scripts:
    - System property for correct Test report handling (e.g Serenity)
    - Initialize jobs from code at startup:
      https://jenkinsci.github.io/job-dsl-plugin/
  - Finally: make job workspaces configurations persistent, add volume
    ```
    docker run --rm --name myjenkins -v myjenkins:/var/jenkins_home -p 8088:8080 myjenkins:lts
    ```
  - Cleanup, beatifying: Create a compose file for easier startup

## [02] Compose version for Docker pipelines

All the jenkins settings are recorded in a [docker-compose.yml](./docker-compose.yml) file next to this README.md, which:
- has access to the Docker engine daemon running on the host via docker.sock and implicit volumes also made available
- has the host's `/var/jenkins_home` to the container's exact same path, to fix docker workspace mount's initiated by Jenkins

Before starting it, the volume permission need some preparation
```
sudo mkdir -p /var/jenkins_home
sudo chown -R 1000:1000 /var/jenkins_home

docker-compose up -d
```

### Example pipeline snippets:

More can be found here: https://jenkins.io/doc/book/pipeline/docker/

A skeleteton pipeline to start with:
```.groovy
pipeline {
    agent any
    stages {
        stage('Example') {
            steps {
                script {
                    echo 'Hello Jenkins!'
                }
            }
        }
    }
}
```

Running a container from an existing image:
```.groovy
def image = docker.image("nginx")
def container = image.run()
sh("docker ps")
container.stop()
```

Same, but shorter:
```.groovy
def container = docker.image("nginx").run()
sh("docker ps")
container.stop()
```

Build images + run container:
```.groovy
git(url: 'https://gitlab.com/zsolooo/docker-training.git')
def commitHash = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()

dir('02_images/02_website') {
  def websiteImage = docker.build("website:${commitHash}")
  def container = websiteImage.run()
  sh("docker ps")
  container.stop()
}
```

Run a container with autostop
```.groovy
docker.image('nginx').withRun('-p 8081:80') { c ->
    sh("docker ps")
    sh("curl http://localhost:8081")
}

// Equivalent commands:
//
// docker run -d -p 8081:80 nginx
// docker ps
// curl http://localhost:8081
// docker stop <containerId>
// docker rm <containerId>
```

Execute Jenkins steps inside a container (Scripted version):
- e.g. to ensure a specific tool with a specific version exist for your job
- to don't depend on what's installed on the Jenkins host

```.groovy
docker.image("maven:3.6-jdk-8-slim").inside("-u root -v ${HOME}/.m2:/root/.m2") {
    dir("02_images/03_springboot/demo") {
        sh("mvn clean package")
    }
}
```

Same, but only for declarative syntax:
```.groovy
pipeline {
    agent any
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.6-jdk-8-slim'
                    args '-u root -v ${HOME}/.m2:/root/.m2'
                }
            }
            steps {
                sh('mvn clean package')
            }
        }
    }
}
```

Using a Docker registry
- authentication info is stored in a Jenkins secret (id: gitlab-credentials)
```
git(url: 'https://gitlab.com/zsolooo/docker-training.git')
def commitHash = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
docker.withRegistry('https://registry.gitlab.com/zsolooo/docker-training', 'gitlab-credentials') {
    dir('02_images/02_website') {
        def websiteImage = docker.build("website:${commitHash}")

        // If image name needs to be changed then manual tagging is needed
        // (websiteImage.tag(newTag) cannot change the full image name)
        sh("docker tag ${websiteImage.id} zsolooo/docker-training:website-${commitHash}")
        
        pushImage = docker.image("zsolooo/docker-training:website-${commitHash}")
        pushImage.push("website-" + commitHash)
        pushImage.push('latest')
    }

    def anotherImage = docker.image('zsolooo/docker-training:myalpine')
    anotherImage.inside {
      sh("cat /etc/os-release")
    }
}
```

Complex example:
- run a container from image built in-place with automatic stopping when no longer needed
- attach another container to the same network and pid namespace (this way no need to publish ports therefore no port collision)
- execute pipeline steps in it (e.g. run tests against the first container with localhost connection)

```.groovy
git(url: 'https://gitlab.com/zsolooo/docker-training.git')
def commitHash = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()

dir('02_images/02_website') {
  docker.build('training-website').withRun { c ->
    echo "Container id: ${c.id}"
    docker.image("alpine").inside("--net=container:${c.id} --pid=container:${c.id}") {
        sh("ps aux")
        sh("apk update")
        sh("apk add curl")
        sh("curl http://localhost:80")
    }
  }
}
```