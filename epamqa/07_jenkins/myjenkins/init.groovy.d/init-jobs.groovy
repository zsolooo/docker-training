import javaposse.jobdsl.dsl.*
import  javaposse.jobdsl.plugin.*

JenkinsJobManagement jm = new JenkinsJobManagement(System.out, [:], new File('.'));
DslScriptLoader dslScriptLoader = new DslScriptLoader(jm)
dslScriptLoader.runScript("folder('Project-A')")
dslScriptLoader.runScript("folder('Project-B')")
dslScriptLoader.runScript("folder('Project-C')")

dslScriptLoader.runScript('''
pipelineJob("test-pipeline") {
  definition {
    cps {
      script("""
        pipeline {
        agent any
            stages {
                stage('Stage 1') {
                    steps {
                        echo 'logic'
                    }
                }
                stage('Stage 2') {
                    steps {
                        echo 'logic'
                    }
                }
            }
        }
      """.stripIndent())
      sandbox()     
    }
  }
}
''')
