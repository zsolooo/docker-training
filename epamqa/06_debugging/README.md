# Debugging containers

## Docker related issues

### Image issues

#### Access denied
```
Using default tag: latest
Error response from daemon: pull access denied for elastic/elastixearch, repository does not exist or may require 'docker login': denied: requested access to the resource is denied
```

What happened, a typo (elasti_x_earch -> elasticsearch):
```
docker pull elastic/elastixearch
```

#### Manifest unknown
```
Unable to find image 'elastic/elasticsearch:latest' locally
docker: Error response from daemon: manifest for elastic/elasticsearch:latest not found: manifest unknown: manifest unknown.
See 'docker run --help'.
```

### Cannot bind port

```
docker: Error response from daemon: driver failed programming external connectivity on endpoint nginx2 (89d2ca282abcd7872f4f89655dbffbc4f9f211f857542caad49e77f5f20db452): Bind for 0.0.0.0:8888 failed: port is already allocated.
```




What happened, but not neccessarily (not just docker can hold a specific host port):
```
docker run --rm -d --name nginx1 -p8888:80 nginx
docker run --rm -d --name nginx2 -p8888:80 nginx
```

## Generic debugging of applications in docker

For testing:
```
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elastic/elasticsearch:7.6.1
```

### Check image/container settings
```
docker inspect elasticsearch
docker inspect elastic/elasticsearch:7.6.1
```

```
docker inspect -f "{{.ContainerConfig.Entrypoint}}" elastic/elasticsearch:7.6.1
docker inspect -f "{{.ContainerConfig.Cmd}}" elastic/elasticsearch:7.6.1
```

https://golang.org/pkg/text/template/

### Container stats

```
docker container stats
```


### Container logs

Container log = The container entrypoint's stdout

```
docker logs elasticsearch
```

When the log is too much (for example you can get the fail reason of a stopped container)
```
docker logs --tail=100 elasticsearch
```

When you're waiting for a specific event (follow):
```
docker logs -f elasticsearch
```

### Step into the container

```
docker exec -ti elasticsearch /bin/sh
```

```
docker run --rm -ti --entrypoint /bin/sh  elastic/elasticsearch:7.6.1
```

## Java application debugging

### JMX (Java Management Extensions)

- JConsole
- VisualVM

```
ES_JAVA_OPTS="-Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=9010 \
    -Dcom.sun.management.jmxremote.rmi.port=9010 \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Dcom.sun.management.jmxremote.local.only=false \
    -Djava.rmi.server.hostname=ubuntu.mshome.net"
    
docker run -d --name elasticsearch \
    -e ES_JAVA_OPTS="$ES_JAVA_OPTS" \
    -p 9200:9200 -p 9300:9300 -p9010:9010 \
    -e "discovery.type=single-node" \
    elastic/elasticsearch:7.6.1
```

http://trustmeiamadeveloper.com/2016/03/18/where-is-my-memory-java/

### JDWP - Java Debug Wire Protocol

https://www.baeldung.com/java-application-remote-debugging


## Advanced debugging

```
docker run --rm -ti --pid container:elasticsearch --net container:elasticsearch ubuntu bash
```