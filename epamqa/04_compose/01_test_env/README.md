# Dockerize Testing infrastructure

## [01] Test environment + extending
- Create an application environment
- Extend it with debugging component
- Extend it with testing

Links:

- https://www.blazemeter.com/blog/jmeter-distributed-testing-with-docker/
- https://docs.docker.com/compose/compose-file/
- https://docs.docker.com/compose/extends/
- https://docs.docker.com/compose/startup-order/

## [02] Jenkins Master (WIP)
- `docker pull jenkins/jenkins:lts`
- https://github.com/jenkinsci/docker/blob/master/README.md
- Create an image based on this with my custom plugins already installed
  ```
  docker build -t myjenkins:lts ./myjenkins

  docker network create -d bridge jenkins
  docker volume create -d local jenkins_master
  
  docker volume create -d local jmeter1

  docker run -d \
    --name jenkins-server \
    --network jenkins \
    -v jenkins_master:/var/jenkins_home \
    -p 8080:8080 \
    jenkins/jenkins:lts

  docker run -d \
    --name jmeter1 \
	--network jenkins \
	-v jmeter1:/mnt/jmeter \
    justb4/jmeter \
	     -n -s \
        -Jserver.rmi.ssl.disable=true \
	    -Jclient.rmi.localport=7000 \
        -Jserver.rmi.localport=60000 \
	    -j /mnt/jmeter/server/slave.log 
  ```

  ```
  jmeter -n -t script.jmx -R jmeter-{01..05}
  ```
