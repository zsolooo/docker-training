package main

import (
	"fmt"
	"time"

	// "sourcegraph.com/sourcegraph/go-selenium"
	"github.com/tebeka/selenium"
)

func main() {
	var err error

	caps := selenium.Capabilities{"browserName": "chrome", "enableVideo": true, "videoScreenSize": "1024x768", "enableLog": true}
	driver, err := selenium.NewRemote(caps, "http://localhost:4444/wd/hub")
	if err != nil {
		fmt.Printf("create selenium session: %v", err)
	}
	defer driver.Quit()

	err = driver.Get("https://zsolooo.gitlab.io/docker-training/")
	if err != nil {
		fmt.Printf("Failed to load page: %s\n", err)
		return
	}

	if title, err := driver.Title(); err == nil {
		fmt.Printf("Page title: %s\n", title)
	} else {
		fmt.Printf("Failed to get page title: %s", err)
		return
	}

	btn, err := driver.FindElement(selenium.ByCSSSelector, "body > div > p:nth-child(3) > asciinema-player > div > div > div.control-bar > span.playback-button")
	if err != nil {
		panic(err)
	}
	fmt.Println("Play button found.")
	if err := btn.Click(); err != nil {
		panic(err)
	}
	fmt.Println("CLicking on Play button.")

	time.Sleep(60 * time.Second)
	fmt.Println("Ending session.")
}
