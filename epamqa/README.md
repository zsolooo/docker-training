# Docker training materials

Terminal recordings are going to be uploaded here: https://zsolooo.gitlab.io/docker-training/epamqa/

## Lessons

1. [Introduction](01_intro/README.md)
1. [Working with Docker images](02_images/README.md)

1. [Jenkins vs Docker](07_jenkins/README.md)

