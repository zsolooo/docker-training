# Working with Docker images [Page is under development]

## Tasks

- For the description of public docker images you can search here: https://hub.docker.com
- Slides bookmark (#104): https://qconsf2017intro.container.training/#104

### [01] Build without Dockerfile (create it from container)

- Create a container: `docker run -ti --name `

### [02] Containerize a static website

**Task:** Create a Dockerfile to build an image which can be used to host the solution web pages of this training (the "public" folder of this repository).

- Use a public webserver image as base image: `nginx:latest`
- Add the contents of the `public` folder into the web root directory (for nginx it's `/usr/share/nginx/html`)

#### Improvement points

- Make the nginx version (tag of the base image) configurable during image build (hint: `--build-arg`)

### [03] Containerize a compiled app

Preparations:
- Download generated demo project (start.spring.io): 
[Demo app](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.2.4.RELEASE&packaging=jar&jvmVersion=1.8&groupId=com.epam.training.docker&artifactId=demo&name=demo&description=Demo%20project%20for%20Spring%20Boot&packageName=com.epam.training.docker.demo&dependencies=actuator)
  (Source will be available in this repo too)

- Do the maven build during the image build

### [04] Reusing images

- https://hub.docker.com/_/postgres


NOTES:
- https://www.learnitguide.net/2018/06/docker-arg-vs-env-command-differences.html