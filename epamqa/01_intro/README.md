# Introduction

## Environment
- You might have Docker Desktop or Docker for Windows already installed, which actually already have a virtual machine installed on HyperV or maybe some older versions on VirtualBox, these could remain unchanged during the training, but I recommend stopping them for the training sessions to free up memory.
- The plan for the training is __not to use Docker Desktop or Docker for Windows__ as it have an extra abstraction layer over the filesystem, which might cause strange issues and can complicate the understanding of docker itself. (Though if we have time at the end of the training I will try to address these differences as well).
- Unfortunately two different Virtualization tools cannot work at the same time, but the VM can be created either with VirtualBox or Hyper-V Manager (Hyper-V has simplified Ubuntu install -> “Quick create”),
- My training environment will be Ubuntu 19.04 on top of Hyper-V with dynamic memory allocation (actual usage is 3-4 GB).
- It is important to have __SSH access to the VM__ from your Host machine!
- Docker installation how-to for Ubuntu(“Install Docker Engine – Community”): https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community 

## Tip: VS Code + SSH remote extension + passwordless login
- https://code.visualstudio.com/
- Extension: "Remote Development"
- [SSH setup](ssh.md)

## Theory
https://qconsf2017intro.container.training/#11

## Tasks

1. [Alpine chroot](01_alpine/README.md)
1. [First containers (#56-#100)](https://qconsf2017intro.container.training/#56)
1. [Portainer](02_portainer/README.md)

## Materials

- https://qconsf2017intro.container.training
- https://zsolooo.gitlab.io/docker-training