# Setup passwordless ssh

```
ssh-keygen -t rsa -b 4096 -f ~/.ssh/local.id_rsa
ssh-copy-id -i ~/.ssh/local.id_rsa <VM-IP-Address>
```

~/.ssh/config :
```
Host ubuntu
  Hostname <VM-IP-Address>
  IdentityFile C:\Users\<Win-Username>\.ssh\local.id_rsa
  User <Ubuntu-Username>
```
(in case of Hyper-V, you will probably need to update the IP address time on each reboot or network change)

then:
```
ssh ubuntu
```
