# Alpine
Alpine Linux is a security-oriented, lightweight Linux distribution based on musl libc and busybox.

Not perfect for everything: https://pythonspeed.com/articles/alpine-docker-python/

## chroot
```
sudo cp alpine-root.tgz /root/

# Do these steps as root user
sudo -i

R=/root/alpine-chroot
mkdir -p $R

# Install Alpine base filesystem to a directory
tar xvzfp /root/alpine-root.tgz -C $R

# (Bind) Mount kernel-provided "virtual" filesystem parts
for D in proc sys dev dev/shm ; do mkdir -p $R/$D ; mount -o bind /$D $R/$D ; done

# for D in proc sys dev dev/shm ; do 
# 	  mkdir -p $R/$D
#     mount -o bind /$D $R/$D
# done

chroot $R /bin/sh

for D in proc sys dev/shm dev ; do mkdir -p $R/$D ; umount $R/$D ; done
```

## Docker

```
docker run --rm alpine
```

## Differences:
```
# Check running processes
pstree

# Check network
ip a

# Check kernel
uname -a

# Check filesystem mounts
mount

# Check user
id
```
